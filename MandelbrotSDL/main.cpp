#include <math.h>
#include <time.h>
#include "SDL.h"
#include "SDL_opengl.h" 

#undef main	// vorherig definierten Einstiegspunkt entfernen

SDL_Surface *screen;		// Grafikreferenz auf Bildschirm
SDL_Surface *gfxbuffer;		// Grafikreferenz auf tempor�ren Grafikpuffer

int FensterBreite = 800;	// Breite der Zeichenfl�che in Pixel
int FensterHoehe = 800;		// H�he der Zeichenfl�che in Pixel

int maxIteration = 100;		// Maximale Anzahl an Iterationen
int maxRadius = 4;			// Maximaler komplexer Betrag
double verschiebung_x = 0;	// Verschiebung auf der X-Achse
double verschiebung_y = 0;	// Verschiebung auf der Y-Achse
double skalierung = 100.0f;	// Skalierung

int farbModus = 0;			// Variable die den Farbmodus festlegt
int modFarbeRot = 2;		// Gibt die Verschiebung der Rot-Kurve der Modulo-Farbfunktion an
int modFarbeGruen = 1;		// Gibt die Verschiebung der Gr�n-Kurve der Modulo-Farbfunktion an
int modFarbeBlau = 1;		// Gibt die Verschiebung der Blau-Kurve der Modulo-Farbfunktion an
bool newRender = true;		// Gibt an, ob eine neue Berechnung angefordert wurde

// Zeichenfl�che leeren (weiss)
void ClearSurface()
{
	SDL_FillRect(gfxbuffer, NULL, SDL_MapRGB(screen->format, 255, 255, 255));
}

// Punkt an gegebener Koordinate mit gegebener Farbe zeichnen
void ZeichnePunkt(int x, int y, int r, int g, int b)
{
	uint32_t *grafikspeicher;			// Speicherbereich f�r Grafikspeicher
	uint32_t farbe;						// Variable f�r Farbwert
	farbe = SDL_MapRGB(gfxbuffer->format, r, g, b);	// Farbwert aus Rot Gr�n Blau generieren
	grafikspeicher = (uint32_t*)gfxbuffer->pixels  + (y * (gfxbuffer->pitch/gfxbuffer->format->BytesPerPixel)) + x;
	*grafikspeicher = farbe;
}

// Zeichenfl�che berechnen
void Render() 
{
	// Zeichenfl�che f�r exklusiven Zugriff sperren
	if(SDL_MUSTLOCK(gfxbuffer)) 
    {
        if(SDL_LockSurface(gfxbuffer) < 0) return;	// Wenn es fehlschl�gt, abbrechen
    }

	if(newRender){		// Wenn eine neue Darstellung angefordert wird
		clock_t start_time = clock();	// Startzeit speichern
		ClearSurface();					// Zeichenfl�che leeren

		double logFarbSkalierung = log(skalierung);		// Logarithmus der Skalierung berechnen, f�r Farbfunktion

		for(int punkt_x = 0; punkt_x < FensterBreite; punkt_x++)
		{
			for(int punkt_y = 0; punkt_y < FensterHoehe; punkt_y++)
			{
				int iteration = 0;		// Iterationsz�hler
				double iteration_n = 0.0f;
				double re_z = 0;		// Realteil von z
				double im_z = 0;		// Imagin�rteil von z
				// Zahl c aus aktuellem Bildpunkt berechnen
				double cx = verschiebung_x - (FensterBreite/(2*skalierung)) + (punkt_x / skalierung);
				double cy = verschiebung_y - (FensterHoehe/(2*skalierung)) + (punkt_y / skalierung);

				int r = 0;
				int g = 0;
				int b = 0;

				// So lange Ausf�hren wie Iterationsz�hler kleiner maximale Iterationen
				// und der Betrag kleiner als maximaler Betrag ist
				while(iteration < maxIteration && sqrt(re_z*re_z + im_z*im_z) < maxRadius)
				{
					double re = re_z*re_z - im_z*im_z + cx;
					double im = 2*re_z*im_z + cy;

					re_z = re;
					im_z = im;

					iteration++;	// Iterationsz�hler um eins hochz�hlen
				}

				// Wenn die maximale Anzahl an Iterationen erreicht wurde,
				// und der Betrag nicht ins Unendliche l�uft, ist der Punkt
				// Teil der Mandelbrot-Menge.
				if(iteration == maxIteration)
				{
					r = 0;
					g = 0;
					b = 0;
					ZeichnePunkt(punkt_x, punkt_y, r, g, b);
				}
				else	// Wenn Punkt nicht Teil der Mandelbrot-Menge ist...
				{
					switch(farbModus)	// ...mit festgelegter Farbfunktion f�rben
					{
						case 1:	// Modulo-F�rbung
							//ZeichnePunkt(punkt_x, punkt_y, (iteration*modFarbeRot) % 255, (iteration*modFarbeGruen) % 255, (iteration*modFarbeBlau) % 255);
							r = (iteration*modFarbeRot) % 255;
							g = (iteration*modFarbeGruen) % 255;
							b = (iteration*modFarbeBlau) % 255;
							ZeichnePunkt(punkt_x, punkt_y, r, g, b);
							break;
						case 2:	// Sinus-F�rbung mit harten Farb�berg�ngen
							//ZeichnePunkt(punkt_x, punkt_y, ((sin((iteration/logFarbSkalierung))+1)/2*255), ((sin((iteration/logFarbSkalierung)+2)+1)/2*255), ((sin((iteration/logFarbSkalierung)+4)+1)/2*255));
							r = ((sin((iteration/logFarbSkalierung))+1)/2*255);
							g = ((sin((iteration/logFarbSkalierung)+2)+1)/2*255);
							b = ((sin((iteration/logFarbSkalierung)+4)+1)/2*255);
							ZeichnePunkt(punkt_x, punkt_y, r, g, b);
							break;
						case 3:	// Sinus-F�rbung mit weichen Farb�berg�ngen
							// "Normalized iteration count algorithm" anwenden
							iteration_n = iteration + (log(log((double)maxRadius)) - log(log(re_z*re_z + im_z*im_z)));
							//ZeichnePunkt(punkt_x, punkt_y, ((sin((iteration_n/logFarbSkalierung))+1)/2*255), ((sin((iteration_n/logFarbSkalierung)+2)+1)/2*255), ((sin((iteration_n/logFarbSkalierung)+4)+1)/2*255));
							r = ((sin((iteration_n/logFarbSkalierung))+1)/2*255);
							g = ((sin((iteration_n/logFarbSkalierung)+2)+1)/2*255);
							b = ((sin((iteration_n/logFarbSkalierung)+4)+1)/2*255);
							ZeichnePunkt(punkt_x, punkt_y, r, g, b);
							break;
						default:
							r = 255;
							g = 255;
							b = 255;
							break;
					}
				}

				double linetolerance = 0.5f / skalierung;
				double ticheight = 0.1f;
				if(cx > -linetolerance && cx < linetolerance || cy > -linetolerance && cy < linetolerance)
				{
					ZeichnePunkt(punkt_x, punkt_y, 255-r, 255-g, 255-b);
				}

				for (int i = -5; i < 5; i++)
				{
					if(cx > -(linetolerance-i) && cx < linetolerance+i && cy < ticheight && cy > -ticheight)
					{
						ZeichnePunkt(punkt_x, punkt_y, 255-r, 255-g, 255-b);
					}

					if(cy > -(linetolerance-i) && cy < linetolerance+i && cx < ticheight && cx > -ticheight)
					{
						ZeichnePunkt(punkt_x, punkt_y, 255-r, 255-g, 255-b);
					}
				}
				
			}
		}
		newRender = false;
		
		// Rechenzeit aus aktueller Zeit minus Startzeit berechnen
		fprintf(stdout, "Rechenzeit: %f sec\n", ((clock() - start_time)/1000.0f));

		// Parameter wie Max. Iterationen, Verschiebung, etc... ausgeben
		fprintf(stdout, "Maximale Iterationen: %u\nVerschiebung X: %f\nVerschiebung Y: %f\nSkalierung: %f\nFarbmodus: %u\n\n", maxIteration, verschiebung_x, verschiebung_y, skalierung, farbModus);
	}


	// Exklusiven Zugriff aufheben
    if(SDL_MUSTLOCK(gfxbuffer)) SDL_UnlockSurface(gfxbuffer);

	// Bildschirm f�r exklusiven Zugriff sperren
	if(SDL_MUSTLOCK(screen)) 
    {
        if(SDL_LockSurface(screen) < 0) return;	// Wenn es fehlschl�gt, abbrechen
    }

	// Zeichenfl�che auf Bildschirm kopieren
	SDL_BlitSurface(gfxbuffer, NULL, screen, NULL);

	// Exklusiven Zugriff auf Bildschirm aufheben
    if(SDL_MUSTLOCK(screen)) SDL_UnlockSurface(screen);

    SDL_Flip(screen);	// Grafikpuffer umschalten
}

// Grafikbibliothek initialisieren
void InitSDL()
{
	fprintf(stdout, "SDL wird initialisiert... ");

	// SDL Initialisieren
	if(SDL_Init(SDL_INIT_VIDEO) != 0){	// Wenn Initialisierung Fehlschl�gt
		fprintf(stderr, "\nSDL konnte nicht initialisiert werden:\n%s\n", SDL_GetError());
		exit(0);
	}

	// Objekt f�r Zugriff auf Bildschirm erzeugen
	if (!(screen = SDL_SetVideoMode(FensterBreite, FensterHoehe, 32, SDL_HWSURFACE)))
	{
		fprintf(stderr, "\nEs konnte kein Screen Handle erzeugt werden:\n%s\n", SDL_GetError());
		SDL_Quit();
		exit(0);
	}

	SDL_WM_SetCaption("Mandelbrot-Menge", 0);

	// Zeichenfl�che erstellen (mit gleichen Eigenschaften wie der Bildschirm)
	gfxbuffer = SDL_CreateRGBSurface(screen->flags, screen->w, screen->h, screen->format->BitsPerPixel, 0, 0, 0, 0);

	fprintf(stdout, "OK\n\n");
}

// Hauptfunktion
int main(int argc, char **argv) 
{
	fprintf(stdout, "MandelbrotSDL\nProgramm zur Erkundung der Mandelbrot-Menge\nErstellt von Stefan Oechslein\nim Rahmen der Maturaarbeit 2013\n");
	fprintf(stdout, "Kompiliert am %s um %s\n\n", __DATE__, __TIME__);
	fprintf(stdout, "Bedienung:\nPfeil Oben:   Verschiebung nach oben\nPfeil Unten:  Verschiebung nach unten\nPfeil Links:  Verschiebung nach links\nPfeil Rechts: Verschiebung nach rechts\n");
	fprintf(stdout, "Q:  Vergroessern\nA:  Verkleinern\nW:  Maximale Iterationszahl erhoehen\nS:  Maximale Iterationszahl verringen\n");
	fprintf(stdout, "F1: Farbmodus: Keine Faerbung\nF2: Farbmodus: Modulo\nF3: Farbmodus: Sinus\nF4: Farbmodus: Sinus mit \"Normalized Iteration Count Algorithm\"\n");
	fprintf(stdout, "1:  Modulo Rot\n2:  Modulo Gelb\n3:  Modulo Gruen\n4:  Modulo Cyan\n5:  Modulo Blau\n");
	fprintf(stdout, "ENTER: Aktuelles Bild speichern\nESC: Programm beenden\n\n");


	InitSDL();

	bool abbruch = false;

	while (!abbruch) // Solange kein Abbruch des Programms angefordert ist
	{
		Render();	// Bild berechnen

		// Tastendr�cke auswerten
		SDL_Event sdlevent;
		while(SDL_PollEvent(&sdlevent))
		{
			if(sdlevent.type == SDL_QUIT)
			{
				abbruch = true;
			}
			if(sdlevent.type == SDL_KEYDOWN)
			{
				switch(sdlevent.key.keysym.sym)
				{
				case SDLK_DOWN:
					verschiebung_y = verschiebung_y + (0.01f / (skalierung / FensterHoehe));
					newRender = true;
					break;
				case SDLK_UP:
					verschiebung_y = verschiebung_y - (0.01f / (skalierung / FensterHoehe));
					newRender = true;
					break;
				case SDLK_RIGHT:
					verschiebung_x = verschiebung_x + (0.01f / (skalierung / FensterBreite));
					newRender = true;
					break;
				case SDLK_LEFT:
					verschiebung_x = verschiebung_x - (0.01f / (skalierung / FensterBreite));
					newRender = true;
					break;
				case SDLK_q:
					skalierung = skalierung * 1.5f;
					newRender = true;
					break;
				case SDLK_a:
					skalierung = skalierung / 1.5f;
					newRender = true;
					break;
				case SDLK_w:
					maxIteration = maxIteration + 100;
					newRender = true;
					break;
				case SDLK_s:
					if(maxIteration > 100)
						maxIteration = maxIteration - 100;
					newRender = true;
					break;
				case SDLK_F1:
					farbModus = 0;
					newRender = true;
					break;
				case SDLK_F2:
					farbModus = 1;
					newRender = true;
					break;
				case SDLK_F3:
					farbModus = 2;
					newRender = true;
					break;
				case SDLK_F4:
					farbModus = 3;
					newRender = true;
					break;
				case SDLK_1:
					modFarbeRot = 2;
					modFarbeGruen = 1;
					modFarbeBlau = 1;
					newRender = true;
					break;
				case SDLK_2:
					modFarbeRot = 2;
					modFarbeGruen = 2;
					modFarbeBlau = 1;
					newRender = true;
					break;
				case SDLK_3:
					modFarbeRot = 1;
					modFarbeGruen = 2;
					modFarbeBlau = 1;
					newRender = true;
					break;
				case SDLK_4:
					modFarbeRot = 1;
					modFarbeGruen = 2;
					modFarbeBlau = 2;
					newRender = true;
					break;
				case SDLK_5:
					modFarbeRot = 1;
					modFarbeGruen = 1;
					modFarbeBlau = 2;
					newRender = true;
					break;
				case SDLK_RETURN:	// aktuelles Bild als Bitmap abspeichern
					char filenameBuffer[50];
					sprintf(filenameBuffer, "mandelbrot_i%u_s%u.bmp", maxIteration, (int)floor(skalierung));
					SDL_SaveBMP(screen, filenameBuffer);
					fprintf(stdout, "Bild gespeichert: %s\n", filenameBuffer);
					break;
				case SDLK_ESCAPE:	// Programm beenden
					abbruch = 1;
					break;
				}
			}
		}
	}

	fprintf(stdout, "Beenden...\n");
	SDL_Quit(); // SDL Beenden

	return 0;
}